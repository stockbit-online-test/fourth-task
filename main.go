package main

import (
	"fmt"
	"sort"
)

func main() {
	arr := []string{"kita", "atik", "tika", "aku", "kua", "makan", "kia"}
	output := findAnagrams(arr)
	fmt.Println(output)
}

type sortedRune []rune

func (s sortedRune) Len() int {
	return len(s)
}

func (s sortedRune) Less(i, j int) bool {
	return s[i] < s[j]
}

func (s sortedRune) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func findAnagrams(arr []string) [][]string {
	anagramMap := make(map[string][]int)
	var anagram [][]string
	tree := &tree{root: &treeNode{}}

	arrLen := len(arr)
	var duplicateStr []string

	for i := 0; i < arrLen; i++ {
		runeCurrent := []rune(arr[i])
		sort.Sort(sortedRune(runeCurrent))
		duplicateStr = append(duplicateStr, string(runeCurrent))
	}

	for i := 0; i < arrLen; i++ {
		anagramMap = tree.insert(duplicateStr[i], i, anagramMap)
	}

	for _, value := range anagramMap {
		var combinedTemp []string
		for i := 0; i < len(value); i++ {
			combinedTemp = append(combinedTemp, arr[value[i]])
		}
		anagram = append(anagram, combinedTemp)
	}
	return anagram
}

type treeNode struct {
	isWord    bool
	childrens [26]*treeNode
}

type tree struct {
	root *treeNode
}

func (t *tree) insert(input string, wordIndex int, anagramMap map[string][]int) map[string][]int {
	inputLen := len(input)
	current := t.root

	for i := 0; i < inputLen; i++ {
		index := input[i] - 'a'
		if current.childrens[index] == nil {
			current.childrens[index] = &treeNode{}
		}
		current = current.childrens[index]
	}
	current.isWord = true
	if anagramMap[input] == nil {
		anagramMap[input] = []int{wordIndex}
	} else {
		anagramMap[input] = append(anagramMap[input], wordIndex)
	}
	return anagramMap
}
